require_relative 'order'
require_relative 'order_item'
require_relative 'product'
require_relative 'tax'
require_relative 'tax_rules'

# print console order information
def print_receipt order
  order.order_items.each { |order_item|
    puts "#{order_item.quantity}, #{order_item.product.name}, #{'%.02f' % order_item.price_after_tax}"
  }

  puts "Sales Taxes: #{'%.02f' % order.sales_tax}"
  puts "Total: #{'%.02f' % order.price_after_tax}"
  puts ""
end

# order 1
order1 = Order.new
product1 = Product.new "book", 12.49, :book, :local
product2 = Product.new "music cd", 14.99, :music, :local
product3 = Product.new "chocolate bar", 0.85, :food, :local
order_item1 = OrderItem.new product1, 1
order_item2 = OrderItem.new product2, 1
order_item3 = OrderItem.new product3, 1
order1.add_order_item order_item1
order1.add_order_item order_item2
order1.add_order_item order_item3
order1.checkout()
# print out receipt for order 1
print_receipt order1

# order 2
order2 = Order.new
product4 = Product.new "imported box of chocolates", 10.00, :food, :imported
product5 = Product.new "imported bottle of perfume", 47.50, :perfume, :imported
order_item4 = OrderItem.new product4, 1
order_item5 = OrderItem.new product5, 1
order2.add_order_item order_item4
order2.add_order_item order_item5
order2.checkout()
# print out receipt for order 2
print_receipt order2

# order 3
order3 = Order.new
product6 = Product.new "imported bottle of perfume", 27.99, :perfume, :imported
product7 = Product.new "bottle of perfume", 18.99, :perfume, :local
product8 = Product.new "packet of headache pills", 9.75, :medical, :local
product9 = Product.new "imported box of chocolates", 11.25, :food, :imported
order_item6 = OrderItem.new product6, 1
order_item7 = OrderItem.new product7, 1
order_item8 = OrderItem.new product8, 1
order_item9 = OrderItem.new product9, 1
order3.add_order_item order_item6
order3.add_order_item order_item7
order3.add_order_item order_item8
order3.add_order_item order_item9
order3.checkout()
# print out receipt for order 3
print_receipt order3

class OrderItem
  attr_accessor :product, :quantity, :sales_tax, :price_after_tax

  def initialize product, quantity
    @product = product
    @quantity = quantity
    @sales_tax = 0.0
    @price_after_tax = 0.0
    @tax_rules = TaxRules.new
  end

  # calculate price and sales tax
  def calculate_price
    # get sales taxes
    taxes = @tax_rules.get_tax_rules_of_product @product    
    total_taxes = 0.0
    taxes.each { |tax| total_taxes += tax.value }

    # round up to nearest 0.05
    @sales_tax = (@product.price_before_tax * @quantity * total_taxes * 20).ceil.to_f / 20
    @price_after_tax = (@product.price_before_tax * @quantity + @sales_tax).round(2)
  end
end

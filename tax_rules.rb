# TaxRules: manage tax rules applied to a product
# Todo: make it singleton
class TaxRules
  # shared taxes data
  @@taxes_data = {}
  @@taxes_data[:vat] = Tax.new "VAT", 0.10
  @@taxes_data[:imported] = Tax.new "Imported", 0.05

  # returns taxes applied to a product
  def get_tax_rules_of_product product
    tax_rules = Array.new

    # VAT tax
    tax_rules.push @@taxes_data[:vat] if 
      !(product.product_type == :book ||
        product.product_type == :food ||
        product.product_type == :medical)

    # imported tax
    tax_rules.push @@taxes_data[:imported] if product.product_origin == :imported

    # returns taxes
    tax_rules
  end
end

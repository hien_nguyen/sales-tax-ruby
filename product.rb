class Product
  attr_accessor :name, :price_before_tax, :product_type, :product_origin
  
  def initialize name, price_before_tax, product_type, product_origin
    @name = name
    @price_before_tax = price_before_tax
    @product_type = product_type
    @product_origin = product_origin
  end
end

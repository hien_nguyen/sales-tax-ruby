require_relative 'spec_helper'

describe Order do
  before :each do
    @order = Order.new
  end

  describe "#new" do
    it "returns a Order object" do
      @order.should be_an_instance_of Order
    end
  end

  describe "#order_items" do
    it "returns the correct order_items" do
      @order.order_items.length.should == 0
    end
  end

  describe "#sales_tax" do
    it "returns the correct sales_tax" do
      @order.sales_tax.should == 0.0
    end
  end

  describe "#price_after_tax" do
    it "returns the correct price_after_tax" do
      @order.price_after_tax.should == 0.0
    end
  end

  it "add an order item" do
    product1 = Product.new "book", 12.49, :book, :local
    order_item1 = OrderItem.new product1, 1
    @order.add_order_item order_item1
    @order.order_items.length.should == 1
  end

  it "add same product" do
    product1 = Product.new "book", 12.49, :book, :local
    order_item1 = OrderItem.new product1, 1
    @order.add_order_item order_item1
    @order.order_items.length.should == 1
    
    product2 = Product.new "book", 12.49, :book, :local
    order_item2 = OrderItem.new product2, 1
    
    @order.add_order_item order_item2
    @order.order_items.length.should == 1
    @order.order_items[0].quantity.should == 2
  end

  it "first order" do
    order = Order.new
    product1 = Product.new "book", 12.49, :book, :local
    product2 = Product.new "music cd", 14.99, :music, :local
    product3 = Product.new "chocolate bar", 0.85, :food, :local
    order_item1 = OrderItem.new product1, 1
    order_item2 = OrderItem.new product2, 1
    order_item3 = OrderItem.new product3, 1
    order.add_order_item order_item1
    order.add_order_item order_item2
    order.add_order_item order_item3
    order.checkout()

    order.sales_tax.should == 1.50
    order.price_after_tax.should == 29.83
  end

  it "second order" do
    order = Order.new
    product1 = Product.new "imported box of chocolates", 10.00, :food, :imported
    product2 = Product.new "imported bottle of perfume", 47.50, :perfume, :imported
    order_item1 = OrderItem.new product1, 1
    order_item2 = OrderItem.new product2, 1
    order.add_order_item order_item1
    order.add_order_item order_item2
    order.checkout()

    order.sales_tax.should == 7.65
    order.price_after_tax.should == 65.15
  end

  it "third order" do
    order = Order.new
    product1 = Product.new "imported bottle of perfume", 27.99, :perfume, :imported
    product2 = Product.new "bottle of perfume", 18.99, :perfume, :local
    product3 = Product.new "packet of headache pills", 9.75, :medical, :local    
    product4 = Product.new "imported box of chocolates", 11.25, :food, :imported
    order_item1 = OrderItem.new product1, 1
    order_item2 = OrderItem.new product2, 1
    order_item3 = OrderItem.new product3, 1
    order_item4 = OrderItem.new product4, 1
    order.add_order_item order_item1
    order.add_order_item order_item2
    order.add_order_item order_item3
    order.add_order_item order_item4
    order.checkout()

    order.sales_tax.should == 6.70
    order.price_after_tax.should == 74.68
  end
end

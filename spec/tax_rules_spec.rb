require_relative 'spec_helper'

describe "Tax Rules" do
  before :each do
    @tax_rules = TaxRules.new
    @vat_tax = 0.10
    @imported_tax = 0.05
  end

  describe "#new" do
    it "returns a TaxRules object" do
      @tax_rules.should be_an_instance_of TaxRules
    end
  end

  it "returns no tax rules for local food/book/medical product" do
    product = Product.new "book", 12.49, :book, :local
    local_tax_rules = @tax_rules.get_tax_rules_of_product product
    local_tax_rules.length.should == 0
  end

  it "returns vat tax rule for local of non food/book/medical product" do
    product = Product.new "music cd", 12.49, :music, :local
    local_tax_rules = @tax_rules.get_tax_rules_of_product product
    local_tax_rules.length.should == 1
    local_tax_rules[0].value.should == @vat_tax
  end

  it "returns imported tax rule for imported of food/book/medical product" do
    product = Product.new "imported box of chocolates", 12.49, :food, :imported
    local_tax_rules = @tax_rules.get_tax_rules_of_product product
    local_tax_rules.length.should == 1
    local_tax_rules[0].value.should == @imported_tax
  end

  it "returns imported and vat tax rule for imported of non food/book/medical product" do
    product = Product.new "imported bottle of perfume", 12.49, :perfume, :imported
    local_tax_rules = @tax_rules.get_tax_rules_of_product product
    local_tax_rules.length.should == 2
    local_tax_rules[0].value.should == @vat_tax
    local_tax_rules[1].value.should == @imported_tax
  end
end

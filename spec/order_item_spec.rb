require_relative 'spec_helper'

describe OrderItem do
  before :each do
    product = Product.new "Name", "PriceBeforeTax", :product_type, :product_origin
    @order_item = OrderItem.new product, 1
  end

  describe "#new" do
    it "takes 2 parameters and returns a OrderItem object" do
      @order_item.should be_an_instance_of OrderItem
    end
  end

  describe "#quantity" do
    it "returns the correct quantity" do
      @order_item.quantity.should == 1
    end
  end

  describe "#sales_tax" do
    it "returns the correct sales_tax" do
      @order_item.sales_tax.should == 0.0
    end
  end

  describe "#price_after_tax" do
    it "returns the correct price_after_tax" do
      @order_item.price_after_tax.should == 0.0
    end
  end

  it "calculate sales tax and price for local food/book/medical product" do
    product1 = Product.new "book", 12.49, :book, :local
    order_item1 = OrderItem.new product1, 1
    order_item1.calculate_price
    order_item1.sales_tax.should == 0.0
    order_item1.price_after_tax.should == 12.49
  end

  it "calculate sales tax and price for local of non food/book/medical product" do
    product1 = Product.new "music cd", 14.99, :music, :local
    order_item1 = OrderItem.new product1, 1
    order_item1.calculate_price
    order_item1.sales_tax.should == 1.50
    order_item1.price_after_tax.should == 16.49
  end

  it "calculate sales tax and price for imported of food/book/medical product" do
    product1 = Product.new "imported box of chocolates", 10.00, :food, :imported
    order_item1 = OrderItem.new product1, 1
    order_item1.calculate_price
    order_item1.sales_tax.should == 0.50
    order_item1.price_after_tax.should == 10.50
  end

  it "calculate sales tax and price for imported of non food/book/medical product" do
    product1 = Product.new "imported bottle of perfume", 47.50, :perfume, :imported
    order_item1 = OrderItem.new product1, 1
    order_item1.calculate_price
    order_item1.sales_tax.should == 7.15
    order_item1.price_after_tax.should == 54.65
  end
end

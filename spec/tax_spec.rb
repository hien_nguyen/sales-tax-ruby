require_relative 'spec_helper'

describe Tax do
  before :each do
    @tax = Tax.new "Name", 0.10
  end

  describe "#new" do
    it "takes 2 parameters and returns a Tax object" do
      @tax.should be_an_instance_of Tax
    end
  end

  describe "#name" do
    it "returns the correct name" do
      @tax.name.should eql "Name"
    end
  end

  describe "#value" do
    it "returns the correct value" do
      @tax.value.should eql 0.10
    end
  end
end

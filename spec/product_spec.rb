require_relative 'spec_helper'

describe Product do
  before :each do
    @product = Product.new "Name", "PriceBeforeTax", :product_type, :product_origin
  end

  describe "#new" do
    it "takes 4 parameters and returns a Product object" do
      @product.should be_an_instance_of Product
    end
  end

  describe "#name" do
    it "returns the correct name" do
      @product.name.should eql "Name"
    end
  end

  describe "#price_before_tax" do
    it "returns the correct price_before_tax" do
      @product.price_before_tax.should eql "PriceBeforeTax"
    end
  end

  describe "#product_type" do
    it "returns the correct product_type" do
      @product.product_type.should eql :product_type
    end
  end

  describe "#product_origin" do
    it "returns the correct product_origin" do
      @product.product_origin.should eql :product_origin
    end
  end
end

class Order
  attr_accessor :order_items, :sales_tax, :price_after_tax

  def initialize
    @order_items = Array.new()
    @sales_tax = 0.0
    @price_after_tax = 0.0    
  end

  # add an order item to shopping basket
  # if add same product, increase the quantity
  def add_order_item order_item
    # check if same product (compare on name): increase quantity, not add new
    # better if compare on product id or product code
    existing_item = @order_items.find { |item| item.product.name == order_item.product.name }

    if existing_item == nil then
      @order_items.push order_item
    else
      existing_item.quantity += order_item.quantity
    end
  end

  # todo: remove an order item

  # checkout the shopping basket
  def checkout
    @order_items.each do |order_item|
      order_item.calculate_price
      @sales_tax += order_item.sales_tax
      @price_after_tax += order_item.price_after_tax
    end

    @sales_tax = @sales_tax.round(2)
    @price_after_tax = @price_after_tax.round(2)
  end
end
